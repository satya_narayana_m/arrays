/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LandGrid;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Satyanarayana Madala
 */
public class LandGrid {

    static int[][] gridArray;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
        Scanner in = new Scanner(new File("gridData.txt"));
        int noOfGrids = 1;
        while (in.hasNext()) {
            int noOfRows = in.nextInt();
            int noOfCols = in.nextInt();
            gridArray = new int[noOfRows][noOfCols];
            for (int i = 0; i < noOfRows; i++) {
                for (int j = 0; j < noOfCols; j++) {
                    gridArray[i][j] = in.nextInt();
                }
            }
            System.out.println("GRID LAYOUT : " + noOfGrids);
            System.out.println("The contents of the grid :");
            for (int i = 0; i < gridArray.length; i++) {
                for (int j = 0; j < gridArray[i].length; j++) {
                    System.out.printf("%3d", gridArray[i][j]);
                }
                System.out.println("");
            }
            System.out.println("");
            averageGridElevation();
            System.out.println("");
            averageRowElevation();
            System.out.println("");
            maxElevation();
            System.out.println("");
            coordinatesOfPeaks();

            noOfGrids++;
        }
    }

    static void averageGridElevation() {
        double total = 0.0;
        int count = 0;
        for (int i = 0; i < gridArray.length; i++) {
            for (int j = 0; j < gridArray[i].length; j++) {
                total += gridArray[i][j];
                count++;
            }
        }
        System.out.printf("Average elevation of the entire grid is %.2f \n", (total / count));
    }

    static void averageRowElevation() {

        for (int i = 0; i < gridArray.length; i++) {
            double total = 0.0;
            for (int j = 0; j < gridArray[i].length; j++) {
                total += gridArray[i][j];
            }
            System.out.printf("Average elevation of row " + (i+1) + " is %.3f \n", (total / gridArray[i].length));
        }

    }

    static void maxElevation() {
        int maxVal = gridArray[0][0];
        for (int i = 0; i < gridArray.length; i++) {
            for (int j = 0; j < gridArray[i].length; j++) {
                if (gridArray[i][j] > maxVal) {
                    maxVal = gridArray[i][j];
                }
            }
        }
        System.out.println("The maximum elevation on the landmass is " + maxVal);
    }

    static void coordinatesOfPeaks() {
        int[][] newGrid = new int[gridArray.length + 2][gridArray[0].length + 2];
        for (int i = 0; i < newGrid.length; i++) {
            for (int j = 0; j < newGrid[i].length; j++) {
                if (i == 0 || i == newGrid.length - 1 || j == 0 || j == newGrid[i].length - 1) {
                    newGrid[i][j] = -1;
                } else {
                    newGrid[i][j] = gridArray[i - 1][j - 1];
                }
            }

        }
        System.out.printf("Co-ordinates of the peaks of elevation: ");
        for (int i = 1; i < newGrid.length - 1; i++) {
            for (int j = 1; j < newGrid[i].length - 1; j++) {
                boolean isTrue = true;

                for (int k = -1; k <= 1; k++) {
                    for (int l = -1; l <= 1; l++) {
                        if (i != i + k || j != j + l) {
                            if (newGrid[i][j] > newGrid[i + k][j + l]) {

                            } else {
                                isTrue = false;
                            }
                        }
                    }
                }

                if (isTrue) {
                    int x = i - 1, y = j - 1;
                    System.out.printf("[" + x + " ," + y + "]");
                }

            }
        }
        System.out.println("");
        System.out.println("");
    }
}
